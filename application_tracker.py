import os
import json
import re
from openai import OpenAI
import logging
from email.message import Message

from config import APP_TRACKER_DATA, APP_TRACKER_GPT_RESPONSES

class ApplicationTracker(object):

    APPLICATION_KEYWORDS = [
        "apply", "appli"
    ]

    logger = logging.getLogger(__name__ + '.ApplicationTracker')

    def __init__(self):
        self.load_applications()

        if not os.path.exists(APP_TRACKER_GPT_RESPONSES):
            os.makedirs(APP_TRACKER_GPT_RESPONSES)
        
    def load_applications(self):
        if os.path.exists(APP_TRACKER_DATA):
            with open(APP_TRACKER_DATA, 'r') as f:
                self.logger.debug(f'Loading application data from {APP_TRACKER_DATA}')
                self.tracker_data = json.load(f)
        else:
            self.logger.debug(f'No application data found at {APP_TRACKER_DATA}')
            self.tracker_data = {
                "known_emails": [],
                "applications": []
            }
        
    def save_applications(self):
        self.logger.debug(f'Saving application data to {APP_TRACKER_DATA}')
        with open(APP_TRACKER_DATA, 'w') as f:
            json.dump(self.tracker_data, f)

    def find_applications(self, emails):
        
        for email_id, email_data in emails.items():

            if email_id in self.tracker_data['known_emails']:
                self.logger.debug(f'Skipping email with id {email_id} as it has already been processed')
                continue

            try:
                is_application, application_data = self.extract_application_data(email_id, email_data)
            except Exception as e:
                self.logger.error(f'Failed to extract application data from email with id {email_id}: {e}')
                continue

            if is_application:
                self.logger.debug(f'Found application in email with id {email_id}')
                self.tracker_data['applications'].append(application_data)

            self.logger.debug(f'Adding email with id {email_id} to known emails')
            self.tracker_data['known_emails'].append(email_id)

            self.save_applications()

    def extract_application_data(self, email_id: str, email_content: Message) -> tuple[bool, dict]:

        client = OpenAI()

        body_has_keyword = any(keyword in email_content.lower() for keyword in self.APPLICATION_KEYWORDS)
        if not body_has_keyword:
            return False, {}
        
        if os.path.exists(f'{APP_TRACKER_GPT_RESPONSES}/{email_id}.json'):
            with open(f'{APP_TRACKER_GPT_RESPONSES}/{email_id}.json', 'r') as f:
                application_data = json.load(f)
                self.logger.debug(f'Loaded application data from GPT response for email with id {email_id}')

        else:
            clean_content = self.strip_email_content(email_content)

            # Use chat GPT to extract application data
            response = client.chat.completions.create(
                model="gpt-4",
                messages=[
                    {"role": "system", "content": "Your role is to help the user track job applications by extracting any relevant information from the emails you receive from users."},
                    {"role": "system", "content": "The bare minimum returned should be the company name and the job title."},
                    {"role": "system", "content": "The answer must be in the form of a JSON object. The keys 'company' and 'job_title' are mandatory. Add any other relevant information as needed under other keys."},
                    {"role": "system", "content": "Some emails might not be of job applications; If the email does not relate to a job application, return an empty JSON object."},
                    {"role": "system", "content": "Do NOT add markdown, do NOT write anything but clean valid parseable JSON. Do NOT add any additional information or context"},
                    {"role": "user", "content": email_content}
                ]
            )
            application_data = response.choices[0].message.content
            self.save_response(email_id, application_data)

        self.logger.debug(f'Extracted application data from email with id {email_id}: {application_data}')

        try:
            application_data_json = json.loads(application_data)
            self.logger.debug(f'Parsed application data from email with id {email_id}')
            if len(application_data_json) == 0:
                return False, {}
            return True, application_data_json
        except json.JSONDecodeError:
            self.logger.warning(f'Failed to parse application data from email with id {email_id}')
            return True, {"raw_data": application_data}

    def save_response(self, email_id: str, response: str) -> None:
        with open(f'{APP_TRACKER_GPT_RESPONSES}/{email_id}.json', 'w') as f:
            f.write(response)

    def strip_email_content(self, email_content: Message) -> str:
        # Remove everything inside angle brackets
        clean_content = re.sub(r'<.*?>', '', email_content)
        return clean_content
