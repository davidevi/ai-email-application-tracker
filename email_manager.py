import os
import imaplib
import email
import logging 
from datetime import datetime

from config import EMAILS_PATH, EMAIL_MANAGER_DATA, REFRESH_FREQUENCY_HOURS

class EmailManager(object):

    logger = logging.getLogger(__name__ + '.EmailManager')

    INVALID_FILENAME_CHARS = ['\n', '<', '>', '@', '.', '/', '\\', '\t', ' ']

    def __init__(self, imap_server, imap_user, imap_pass):
        self.mail = imaplib.IMAP4_SSL(imap_server)
        self.mail.login(imap_user, imap_pass)
        self.mail.select('inbox')

        self.emails = {}

        if not os.path.exists(EMAILS_PATH):
            os.makedirs(EMAILS_PATH)

    def __del__(self):
        self.mail.close()
        self.mail.logout()

    def load_emails(self):
        # if last refresh was more than 1 hour ago, refresh emails
        last_refresh = self.get_last_refresh()
        last_refresh_over_threshold = last_refresh is not None and (datetime.now() - last_refresh).seconds > REFRESH_FREQUENCY_HOURS * 3600
        local_has_emails = len(os.listdir(EMAILS_PATH)) > 0

        if not local_has_emails or last_refresh_over_threshold:
            self.logger.debug('Refreshing emails')
            self._load_emails_from_server()
            self.save_last_refresh()
        else:
            self.logger.debug('Loading emails from local')
            self._load_emails_from_local()

    def _load_emails_from_local(self):
        for email_file in os.listdir(EMAILS_PATH):
            self.logger.debug(f'Loading email from local: {email_file}')
            with open(f'{EMAILS_PATH}/{email_file}', 'r') as f:
                self.emails[email_file] = email.message_from_string(f.read())

    def _load_emails_from_server(self):

        _, data = self.mail.search(None, 'ALL')
        total_emails_count = len(data[0].split())

        self.logger.debug(f'Found {total_emails_count} emails')

        if len(os.listdir(EMAILS_PATH)) == total_emails_count:
            self.logger.debug('No changes to emails, skipping download')
            self._load_emails_from_local()
            return

        current_index = 0

        for num in data[0].split():
            _, data = self.mail.fetch(num, '(RFC822)')
            raw_email = data[0][1]
            msg = email.message_from_bytes(raw_email)
            id = msg['Message-ID']

            filesystem_safe_id = id.strip()

            for char in self.INVALID_FILENAME_CHARS:
                filesystem_safe_id = filesystem_safe_id.replace(char, '_')

            # message_content = ""

            # if msg.is_multipart():
            #     for part in msg.walk():
            #         ctype = part.get_content_type()
            #         cdispo = str(part.get('Content-Disposition'))

            #         if ctype == 'text/plain' and 'attachment' not in cdispo:
            #             message_content += part.get_payload()
            # else:
            #     message_content = msg.get_payload()

            # self.emails[filesystem_safe_id] = message_content
            self.emails[filesystem_safe_id] = msg
            self.logger.debug(f'Processed email {current_index}/{total_emails_count} with id: {filesystem_safe_id}')
            current_index += 1

            if not os.path.exists(f'emails/{filesystem_safe_id}.eml'):
                with open(f'{EMAILS_PATH}/{filesystem_safe_id}.eml', 'w') as f:
                    f.write(msg.as_string())

    def save_last_refresh(self):
        """
        Save the last time the emails were refreshed
        """ 
        with open(EMAIL_MANAGER_DATA, 'w') as f:
            f.write(str(datetime.now()))

    def get_last_refresh(self):
        """
        Get the last time the emails were refreshed
        """
        if not os.path.exists(EMAIL_MANAGER_DATA):
            return None
        
        with open(EMAIL_MANAGER_DATA, 'r') as f:
            return datetime.fromisoformat(f.read())