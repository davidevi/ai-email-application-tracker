import os
import logging

from dotenv import load_dotenv
from ddv.logging import log_to_stdout

from email_manager import EmailManager
from application_tracker import ApplicationTracker


def main():
    
    email_manager = EmailManager(
        os.environ.get('IMAP_SERVER'),
        os.environ.get('IMAP_USER'),
        os.environ.get('IMAP_PASS')
    )
    application_tracker = ApplicationTracker()

    email_manager.load_emails()
    application_tracker.find_applications(email_manager.emails)

    for application in application_tracker.tracker_data['applications']:
        print(application)
        print()

if __name__ == "__main__":
    load_dotenv()
    log_to_stdout(
        logging_level=logging.DEBUG,
        enable_colours=True,
        enable_indentation=False, 
        verbosity_level=0,
        verbosity_filters={
            0: [],
            1: ['httpx', 'urllib3', 'httpcore', 'openai'],
        },
    )
    main()
