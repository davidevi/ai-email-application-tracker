
DATA_PATH = "data"
EMAILS_PATH = f"{DATA_PATH}/emails"
EMAIL_MANAGER_DATA = f"{DATA_PATH}/email_manager_data.json"
APP_TRACKER_DATA = f"{DATA_PATH}/app_tracker_data.json"
APP_TRACKER_GPT_RESPONSES = f"{DATA_PATH}/app_tracker_gpt_responses"
REFRESH_FREQUENCY_HOURS = 24